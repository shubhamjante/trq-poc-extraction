from modules.ocr import GoogleVisionAPI
import string
import logging
from modules import preprocessing
logger = logging.getLogger('docbyte')


def __fn(image, coordinates):
    text = GoogleVisionAPI().get_text_from_co_ordinates(image, *coordinates)
    return text


def __remove_sybols(text):
    for char in string.punctuation:
        if char in text:
            text = text.replace(char, '')
    return text


def extract_head(image, coordinates):
    data = {key: __fn(image, coordinates[key]['co-ordinates']) for key in coordinates.keys()}

    data = {key: ' '.join(data[key].values()).lower() for key in data.keys()}
    logger.info("OCR output after removing nested keys: {}".format(data))

    try:
        i = data['date'].find('acid')
        data['date'] = data['date'][i+8:].strip()
    except KeyError:
        data['date'] = 'No country found'

    try:
        i = data['location'].lower().find('local')
        data['location'] = data['location'][i+5:].strip()
        data['location'] = __remove_sybols(data['location'])
    except KeyError:
        data['location'] = 'No location found'

    try:
        con = list()
        for word in data['country'].split():
            if word.startswith('localiz') or word.startswith('pais') or word.startswith('país'):
                continue
            elif not word.isalpha():
                continue
            else:
                con.append(word)

        if len(con) != 0:
            data['country'] = ' '.join(con)
            data['country'] = preprocessing.get_similar_country(data['country'])
        else:
            data['country'] = 'No country found'

        data['country'] = __remove_sybols(data['country'])
    except KeyError:
        data['country'] = 'No country found'

    data = {key: val.strip() for key, val in data.items()}
    logger.info("Header ICR output: {}".format(data))
    return data
