from google.oauth2 import service_account
from google.cloud import vision
from google.cloud.vision import types
from google.cloud import vision_v1
import os
import io
from modules import preprocessing
import string


class GoogleVisionAPI:
    """
    The GoogleVisionAPI object will be used to call Google OCR service.
    Every object has inbuilt Google Vision API credentials by default. You don't have to provide additional credentials.
    Every object contains Google Vision credentials provided by the docbyte. As per the docbyte's convenience the
    default language for OCR is portuguese.

    Args:
          file_path (str): The relative/absolute image file path.
          language_hints (str): Specify the language code to use that language to extract information from an image. The
          language code should be supported by the google. For more information please visit: https://cloud.google.com/vision/docs/languages
          language_hints (default): pt

    """
    def __init__(self, file_path=None, language_hints='pt'):
        self.__API_CREDENTIALS = './modules/credentials/google_vision.json'
        self.__credentials = service_account.Credentials.from_service_account_file(self.__API_CREDENTIALS)
        self.__language = 'en'
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = self.__API_CREDENTIALS

        self.file_path = file_path
        # if not os.path.isfile(file_path):
        #     raise Exception(f"File Not Found. Please check given path: {os.path.abspath(self.file_path)}")

    def __modify_api_responce(self, res):
        block_level_data = list()
        for page in res.full_text_annotation.pages:
            for block in page.blocks:
                for paragraph in block.paragraphs:
                    para = list()
                    for word in paragraph.words:
                        word_text = ''.join([symbol.text for symbol in word.symbols])
                        para.append(word_text)

                    block_level_data.append(' '.join(para))

        data = dict(enumerate(block_level_data))

        for key, value in data.items():
            data[key] = str(value).strip().replace('.', '')
        return data

    def get_text(self):
        """
        This method will create a Google Vision API client and and pass given image to Vision API.
        Returns:
            dict: It contains the block_number and text as key-value pair.

        Raises:
             InvalidMethodCall: If the given file_path is not given.

        Example:
            {
            0: "text 01",
            1: "text 02
            }
        """
        if self.file_path is None or not os.path.isfile(self.file_path):
            raise Exception("Invalid method call. Please provide image file path.")

        client = vision_v1.ImageAnnotatorClient()
        with io.open(self.file_path, 'rb') as img:
            content = img.read()
            image = types.Image(content=content)
            image_context = types.ImageContext(language_hints=[self.__language])
            response = client.document_text_detection(image=image, image_context=image_context)

        return self.__modify_api_responce(response)

    def get_text_from_co_ordinates(self, cv2_image, xmin, ymin, xmax, ymax):
        binary_data = preprocessing.crop_binary_image_data(cv2_image, xmin, ymin, xmax, ymax)

        client = vision_v1.ImageAnnotatorClient()
        image = types.Image(content=binary_data)
        image_context = types.ImageContext(language_hints=[self.__language])
        response = client.document_text_detection(image=image, image_context=image_context)

        return self.__modify_api_responce(response)

    def get_text_full_image(self, image):
        binary_data = preprocessing.get_binary_image(image)

        client = vision_v1.ImageAnnotatorClient()
        image = types.Image(content=binary_data)
        image_context = types.ImageContext(language_hints=[self.__language])
        response = client.document_text_detection(image=image, image_context=image_context)

        return self.__modify_api_responce(response)

# if __name__ == '__main__':
#     a = GoogleVisionAPI('./references/reference_image.png')
#     print(a.get_text())