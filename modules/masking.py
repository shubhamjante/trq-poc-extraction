import cv2
from modules.ocr import GoogleVisionAPI
from modules import preprocessing


def __increase_brightness(image, value):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    alpha = 1.5 # Contrast control (1.0-3.0)
    beta = 1.3 # Brightness control (0-100)

    adjusted = cv2.convertScaleAbs(img, alpha, beta)

    # cv2.imshow('original', image)
    # cv2.imshow('adjusted', adjusted)
    cv2.waitKey(0)

    return adjusted


def __mask_image(image, reference):
    image = cv2.resize(image, reference.shape[1::-1])
    dst = cv2.addWeighted(image, 0.5, reference, 0.5, 0)
    dst = cv2.addWeighted(image, 0.5, reference, 0.2, 128)
    dst = cv2.bitwise_and(image, reference)
    return dst


def __segment_image(image, count=3):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    contours, hierarchy = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2:]
    contours = sorted(contours, key=lambda ctr: cv2.contourArea(ctr), reverse=True)[:count]
    segments = []

    for i, cnt in enumerate(contours):
        x, y, w, h = cv2.boundingRect(cnt)
        roi = image[y:y + h, x:x + w]
        segments.append(roi)
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    return segments, contours


def __fn(image):
    text = GoogleVisionAPI().get_text_full_image(image)
    return ' '.join([words for words in text.values()])


def get_masks(image, mask, image_uuid, party):
    masks = {
        'vehicle_left': {
            'path': cv2.imread('./modules/references/masks/vehicle_left_mask.png'),
            'roi_count': 3,
            'keys': ('make', 'registration_number', 'country')
        },
        'insurance_left': {
            'path': cv2.imread('./modules/references/masks/insurance_left_mask.png'),
            'roi_count': 2,
            'keys': ('policy_number', 'name')
        }
    }

    # corrected_image = __increase_brightness(image, 20)

    masked_image = __mask_image(image, masks[mask]['path'])
    segments, contours = __segment_image(masked_image, masks[mask]['roi_count'])
    results = dict(zip(masks[mask]['keys'], segments))
    for key, value in results.items():
        preprocessing.log_image(value, image_uuid, key + '_' + party)

    output = {key: __fn(img) for key, img in results.items()}
    print(f'Output Masking.py: {output}')

    # output['country'] = preprocessing.get_similar_country(output['country'])
    # output['make'] = preprocessing.get_similar_car_make_type(output['make'])

    print(f"Party Extraction output: {'make' in list(output.keys())}")

    if 'country' in list(output.keys()):
        output['country'] = preprocessing.get_similar_country(output['country'])
    elif 'make' in list(output.keys()):
        # print('make loop')
        output['make'] = preprocessing.get_similar_car_make_type(output['make'])

    return output



