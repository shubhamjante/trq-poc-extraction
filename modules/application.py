from modules.segmentation.level_01.blocks import Level01Segmentation
from modules.segmentation.level_02.party_blocks import extract_party_blocks
from modules.segmentation.middle_section import checkboxes
from modules.segmentation.signature_section.inference import Signature
from modules.segmentation.header_section.header import extract_header
from modules import extract_results
from modules import preprocessing
from modules.masking import get_masks
import logging
import time
logger = logging.getLogger('docbyte')
__results = dict()
__image_uuid = None


def __identify_L1_blocks(image):
    t1 = time.time()
    blocks = Level01Segmentation(image).predict()
    logger.info("Time required for Level 01 Segmentation: {}".format(time.time() - t1))

    return blocks


def __extract_header_fields(head_image):
    t1 = time.time()
    header_coordinates = extract_header(head_image)
    keys = list(header_coordinates.keys())
    coords = [tuple(value['co-ordinates']) for key, value in header_coordinates.items()]

    for key, coord in list(zip(keys, coords)):
        img = preprocessing.crop_image_with_co_ordinates(head_image, *coord)
        preprocessing.log_image(img, __image_uuid, key)

    header_text = extract_results.extract_head(head_image, header_coordinates)
    __results['header'] = header_text

    logger.info("Time required for Header Extraction: {}".format(time.time() - t1))


def __extract_party_A(left_image):
    t1 = time.time()
    vehicle = insurance = None

    party_a_coordinates = extract_party_blocks(left_image)
    try:
        vehicle = preprocessing.crop_image_with_co_ordinates(left_image,
                                                             *party_a_coordinates['vehicle']['co-ordinates'])
        preprocessing.log_image(vehicle, __image_uuid, 'party_A_vehicle')
    except KeyError:
        __results['party_a']['vehicle'] = {'error': 'Level 02 segmentation co-ordinates not found'}
        logger.error('Level 02 segmentation label vehicle not found.')

    try:
        insurance = preprocessing.crop_image_with_co_ordinates(left_image,
                                                               *party_a_coordinates['insurance']['co-ordinates'])
        preprocessing.log_image(insurance, __image_uuid, 'party_A_insurance')
    except KeyError:
        __results['party_a']['insurance'] = {'error': 'Level 02 segmentation co-ordinates not found'}
        logger.error('Level 02 segmentation label insurance not found.')

    __results['party_a'] = {
        'vehicle': get_masks(vehicle, 'vehicle_left', __image_uuid, 'A'),
        'insurance': get_masks(insurance, 'insurance_left', __image_uuid, 'A')
    }

    logger.info("Time required for Party A Extraction: {}".format(time.time() - t1))


def __extract_party_B(right_image):
    t1 = time.time()
    vehicle = insurance = None

    party_b_coordinates = extract_party_blocks(right_image)
    try:
        vehicle = preprocessing.crop_image_with_co_ordinates(right_image,
                                                             *party_b_coordinates['vehicle']['co-ordinates'])
        preprocessing.log_image(vehicle, __image_uuid, 'party_B_vehicle')
    except KeyError:
        __results['party_b']['vehicle'] = {'error': 'Level 02 segmentation co-ordinates not found'}
        logger.error('Level 02 segmentation label vehicle not found.')

    try:
        insurance = preprocessing.crop_image_with_co_ordinates(right_image,
                                                           *party_b_coordinates['insurance']['co-ordinates'])
        preprocessing.log_image(insurance, __image_uuid, 'party_B_insurance')
    except KeyError:
        __results['party_b']['insurance'] = {'error': 'Level 02 segmentation co-ordinates not found'}
        logger.error('Level 02 segmentation label insurance not found.')

    __results['party_b'] = {
        'vehicle': get_masks(vehicle, 'vehicle_left', __image_uuid, 'B'),
        'insurance': get_masks(insurance, 'insurance_left', __image_uuid, 'B')
    }

    logger.info("Time required for Party A Extraction: {}".format(time.time() - t1))


def extract_content(image, image_uuid):
    global __image_uuid
    __image_uuid = image_uuid
    blocks = __identify_L1_blocks(image)

    try:
        head = preprocessing.crop_image_with_co_ordinates(image, *blocks['Top']['co-ordinates'])
        preprocessing.log_image(head, __image_uuid, 'header')
        __extract_header_fields(head)
    except KeyError:
        __results['Top'] = {'error': 'Level 01 segmentation co-ordinate not found'}
        logger.error('Level 01 segmentation label Top not found.')

    try:
        t1 = time.time()
        chkboxes = checkboxes.main(image, blocks['Middle']['co-ordinates'], __image_uuid)
        __results['checkboxes'] = chkboxes
        logger.info("Time required for Checkbox model: {}".format(time.time() - t1))
    except KeyError:
        __results['Middle'] = {'error': 'Level 01 segmentation co-ordinate not found'}
        logger.error('Level 01 segmentation label Middle not found.')

    try:
        party_A = preprocessing.crop_image_with_co_ordinates(image, *blocks['Left']['co-ordinates'])
        preprocessing.log_image(party_A, __image_uuid, 'party_A')
        __extract_party_A(party_A)
    except Exception as E:
        print(f"Party A Exception: {E}")
        __results['party_a'] = {'error': 'Level 01 segmentation co-ordinate not found'}
        logger.error('Level 01 segmentation label Left not found.')

    try:
        party_B = preprocessing.crop_image_with_co_ordinates(image, *blocks['Right']['co-ordinates'])
        preprocessing.log_image(party_B, __image_uuid, 'party_B')
        __extract_party_B(party_B)
    except Exception as E:
        print(f"Party B Exception: {E}")
        __results['party_b'] = {'error': 'Level 01 segmentation co-ordinate not found'}
        logger.error('Level 01 segmentation label Right not found.')

    try:
        t1 = time.time()
        sign = Signature(image, blocks['Down']['co-ordinates'], __image_uuid, para='full')
        __results['sign'] = sign

        logger.info("Time required for signature Segmentation: {}".format(time.time() - t1))
    except KeyError:
        __results['sign'] = {'error': 'Level 01 segmentation co-ordinate not found'}
        logger.error('Level 01 segmentation label Right not found.')

    return __results
