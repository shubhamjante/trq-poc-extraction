import numpy as np
# import sys
import tensorflow as tf
# from modules.segmentation.header_section.utils import ops as utils_ops
from modules.segmentation.header_section.utils import label_map_util
from modules.segmentation.header_section.utils import visualization_utils as vis_util
# from modules import preprocessing
import cv2


def unnormalise_cordinates(normalised_cordinates_list, height, width):
    ymin = int(normalised_cordinates_list[0]*height)
    xmin = int(normalised_cordinates_list[1]*width)
    ymax = int(normalised_cordinates_list[2]*height)
    xmax = int(normalised_cordinates_list[3]*width)
    return [xmin, ymin, xmax, ymax]


def get_actual_coordinates(data, image_height, image_width):
    for key in data.keys():
        data[key]['co-ordinates'] = unnormalise_cordinates(data[key]['co-ordinates'], image_height, image_width)
    return data


def extract_header(image):
    MODEL_PATH = './modules/segmentation/header_section/models/frozen_inference_graph.pb'
    LABEL_PATH = './modules/segmentation/header_section/models/label_map.pbtxt'
    NUM_CLASSES = 3

    height, width = image.shape[:2]

    label_map = label_map_util.load_labelmap(LABEL_PATH)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.compat.v1.GraphDef()
        with tf.io.gfile.GFile(MODEL_PATH, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        sess = tf.compat.v1.Session(graph=detection_graph)

    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    image_expanded = np.expand_dims(image, axis=0)

    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    img, my_map = vis_util.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=1,
        min_score_thresh=0.70)

    # cv2.imwrite('header.png', img)
    # cv2.waitKey(0)

    normalized_co_ordinates = {}
    for key, val in my_map.items():
        label = val[0].split(':')[0]
        score = val[0].split(':')[1]
        normalized_co_ordinates[label] = {'score': score, 'co-ordinates': key}

    # print(normalized_co_ordinates)
    actual_co_ordinates = get_actual_coordinates(normalized_co_ordinates, height, width)

    return actual_co_ordinates
