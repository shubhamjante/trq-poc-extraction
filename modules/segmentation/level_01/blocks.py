import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import imutils

from modules.segmentation.level_01.object_detection.utils import label_map_util
from modules.segmentation.level_01.object_detection.utils import visualization_utils as vis_util

import os
# os.environ["CUDA_VISIBLE_DEVICES"]="-1"


class Level01Segmentation:
    def __init__(self, cv2_image):
        # self.image_path = image_path
        self.__model_path = './modules/segmentation/level_01/models/frozen_inference_graph.pb'
        self.__label_path = './modules/segmentation/level_01/models/label_map.pbtxt'
        self.__num_classes = 5

        self.image = cv2_image
        self.__image_height, self.__image_width, _ = self.image.shape

    def __unnormalise_cordinates(self, normalised_cordinates_list, height, width):
        ymin = int(normalised_cordinates_list[0] * height)
        xmin = int(normalised_cordinates_list[1] * width)
        ymax = int(normalised_cordinates_list[2] * height)
        xmax = int(normalised_cordinates_list[3] * width)

        return xmin, ymin, xmax, ymax

    def __get_actual_coordinates(self, data):
        for key in data.keys():
            data[key]['co-ordinates'] = self.__unnormalise_cordinates(data[key]['co-ordinates'], self.__image_height, self.__image_width)

        return data
    
    def predict(self):
        label_map = label_map_util.load_labelmap(self.__label_path)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=self.__num_classes,
                                                                    use_display_name=True)
        category_index = label_map_util.create_category_index(categories)

        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(self.__model_path, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

            sess = tf.compat.v1.Session(graph=detection_graph)

        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: np.expand_dims(self.image, axis=0)})

        img, my_map = vis_util.visualize_boxes_and_labels_on_image_array(
            self.image,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=1,
            min_score_thresh=0.60)

        normalized_co_ordinates = {}
        for key, val in my_map.items():
            label = val[0].split(':')[0]
            score = val[0].split(':')[1]
            normalized_co_ordinates[label] = {'score': score, 'co-ordinates': key}

        actual_co_ordinates = self.__get_actual_coordinates(normalized_co_ordinates)

        return actual_co_ordinates


# if __name__ == '__main__':
#     obj = PartySegmentation('./data/test.png').predict()
#     print(obj)