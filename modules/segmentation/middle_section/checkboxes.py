from modules import preprocessing
from modules.segmentation.middle_section import prediction
import cv2
import logging
import time
from modules import preprocessing
logger = logging.getLogger('docbyte')


def cropMe(im, coords):
    # Crop the iceberg portion
    cropped = im[coords[1]:coords[3], coords[0]:coords[2]].copy()
    return cropped


def main(img, coords, image_uuid):
    t1 = time.time()
    cropped = cropMe(img, coords)
    resized_img = cv2.resize(cropped, (460, 1395))

    height, width, channel = resized_img.shape
    resized_img = resized_img[0:1268, 0:width]
    cropped = cv2.resize(resized_img, (200, 547))

    overlayed_img = preprocessing.middle_part_white_overlay(cropped)
    preprocessing.log_image(overlayed_img, image_uuid, 'checkbox_overlay')

    chk = prediction.predict(overlayed_img)

    try:
        chk['A'] = list(set(chk['A']))
        chk['A'].remove(0)
    except Exception as E:
        print(f'Exception: {E}')

    try:
        chk['B'] = list(set(chk['B']))
        chk['B'].remove(0)
    except Exception as E:
        print(f'Exception: {E}')

    logger.info("Detected checkboxes: {}".format(chk))
    logger.info("Time required for checkboxes: {}".format(time.time() - t1))
    return {key:list(set(val)) for key, val in chk.items()}
