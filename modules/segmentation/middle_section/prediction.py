from __future__ import division
import os
import cv2
import numpy as np
import sys
import pickle
from optparse import OptionParser
import time
from keras_frcnn import config
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras_frcnn import roi_helpers
from modules.segmentation.middle_section.checkbox_classifier.predict import get_label
import logging
logger = logging.getLogger('docbyte')

sys.setrecursionlimit(40000)


def format_img_size(img, C):
    """ formats the image size based on config """
    img_min_side = float(C.im_size)
    (height, width, _) = img.shape

    if width <= height:
        ratio = img_min_side / width
        new_height = int(ratio * height)
        new_width = int(img_min_side)
    else:
        ratio = img_min_side / height
        new_width = int(ratio * width)
        new_height = int(img_min_side)
    img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
    return img, ratio


def format_img_channels(img, C):
    """ formats the image channels based on config """
    img = img[:, :, (2, 1, 0)]
    img = img.astype(np.float32)
    img[:, :, 0] -= C.img_channel_mean[0]
    img[:, :, 1] -= C.img_channel_mean[1]
    img[:, :, 2] -= C.img_channel_mean[2]
    img /= C.img_scaling_factor
    img = np.transpose(img, (2, 0, 1))
    img = np.expand_dims(img, axis=0)
    return img


def format_img(img, C):
    """ formats an image for model prediction based on config """
    img, ratio = format_img_size(img, C)
    img = format_img_channels(img, C)
    return img, ratio


# Method to transform the coordinates of the bounding box to its original size
def get_real_coordinates(ratio, x1, y1, x2, y2):
    real_x1 = int(round(x1 // ratio))
    real_y1 = int(round(y1 // ratio))
    real_x2 = int(round(x2 // ratio))
    real_y2 = int(round(y2 // ratio))

    return (real_x1, real_y1, real_x2, real_y2)


def gettext_old(x, y, img):
    h, w, c = img.shape
    cnt = 0
    if (x > w / 2):
        party = "B"
    else:
        party = "A"

    if y > 57 and y < 67:
        cnt = 1
        # return cnt, party
    elif y > 69 and y < 79:
        cnt = 2
        # return cnt, party
    elif y > 100 and y < 110:
        cnt = 3
        # return cnt, party
    elif y > 119 and y < 129:
        cnt = 4
        # return cnt, party
    elif y > 150 and y < 160:
        cnt = 5
        # return cnt, party
    elif y > 181 and y < 191:
        cnt = 6
        # return cnt, party
    elif y > 212 and y < 222:
        cnt = 7
        # return cnt, party
    elif y > 243 and y < 253:
        cnt = 8
        # return cnt, party
    elif y > 286 and y < 296:
        cnt = 9
        # return cnt, party
    elif y > 316 and y < 326:
        cnt = 10
        # return cnt, party
    elif y > 335 and y < 345:
        cnt = 11
        # return cnt, party
    elif y > 354 and y < 364:
        cnt = 12
        # return cnt, party
    elif y > 373 and y < 383:
        cnt = 13
        # return cnt, party
    elif y > 390 and y < 400:
        cnt = 14
        # return cnt, party
    elif y > 410 and y < 420:
        cnt = 15
        # return cnt, party
    elif y > 454 and y < 464:
        cnt = 16
        # return cnt, party
    elif y > 483 and y < 493:
        cnt = 17
        # return cnt, party

    return cnt, party


def gettext(x, y, img):
    h, w, c = img.shape
    cnt = 0
    if (x > w/2):
        party = "B"
    else:
        party = "A"

    if y > 57 and y < 69:
        cnt = 1
        # return cnt, party
    elif y > 69 and y < 91:
        cnt = 2
        # return cnt, party
    elif y > 91 and y < 116:
        cnt = 3
        # return cnt, party
    elif y > 116 and y < 141:
        cnt = 4
        # return cnt, party
    elif y > 141 and y < 171:
        cnt = 5
        # return cnt, party
    elif y > 171 and y < 203:
        cnt = 6
        # return cnt, party
    elif y > 203 and y < 234:
        cnt = 7
        # return cnt, party
    elif y > 234 and y < 271:
        cnt = 8
        # return cnt, party
    elif y > 271 and y < 307:
        cnt = 9
        # return cnt, party
    elif y > 307 and y < 331:
        cnt = 10
        # return cnt, party
    elif y > 331 and y < 351:
        cnt = 11
        # return cnt, party
    elif y > 351 and y < 370:
        cnt = 12
        # return cnt, party
    elif y > 370 and y < 388:
        cnt = 13
        # return cnt, party
    elif y > 388 and y < 407:
        cnt = 14
        # return cnt, party
    elif y > 407 and y < 439:
        cnt = 15
        # return cnt, party
    elif y > 439 and y < 474:
        cnt = 16
        # return cnt, party
    elif y > 474 and y < 403:
        cnt = 17
        # return cnt, party

    return cnt, party


def predict(img):
    """
    Checkbox detection and classification.
    """
    logger.info("Checkbox detection started.")
    checkboxes = {
        'A': list(),
        'B': list()
    }
    try:
        K.clear_session()

        boxes = []
        parties = []
        labels = []
        config_output_filename = "./modules/segmentation/middle_section/config.pickle"

        with open(config_output_filename, 'rb') as f_in:
            C = pickle.load(f_in)

        if C.network == 'resnet50':
            import keras_frcnn.resnet as nn
        elif C.network == 'vgg':
            import keras_frcnn.vgg as nn

        # turn off any data augmentation at test time
        C.use_horizontal_flips = False
        C.use_vertical_flips = False
        C.rot_90 = False
        C.model_path = "./modules/segmentation/middle_section/Models/modelafterepoch205.hdf5"
        class_mapping = C.class_mapping

        if 'bg' not in class_mapping:
            class_mapping['bg'] = len(class_mapping)

        class_mapping = {v: k for k, v in class_mapping.items()}
        class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}
        C.num_rois = 32

        if C.network == 'resnet50':
            num_features = 1024
        elif C.network == 'vgg':
            num_features = 512

        if K.image_dim_ordering() == 'th':
            input_shape_img = (3, None, None)
            input_shape_features = (num_features, None, None)
        else:
            input_shape_img = (None, None, 3)
            input_shape_features = (None, None, num_features)

        img_input = Input(shape=input_shape_img)
        roi_input = Input(shape=(C.num_rois, 4))
        feature_map_input = Input(shape=input_shape_features)

        # define the base network (resnet here, can be VGG, Inception, etc)
        shared_layers = nn.nn_base(img_input, trainable=True)

        # define the RPN, built on the base layers
        num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
        rpn_layers = nn.rpn(shared_layers, num_anchors)

        classifier = nn.classifier(feature_map_input, roi_input, C.num_rois, nb_classes=len(class_mapping), trainable=True)

        model_rpn = Model(img_input, rpn_layers)
        model_classifier_only = Model([feature_map_input, roi_input], classifier)

        model_classifier = Model([feature_map_input, roi_input], classifier)

        print('Loading weights from {}'.format(C.model_path))
        model_rpn.load_weights(C.model_path, by_name=True)
        model_classifier.load_weights(C.model_path, by_name=True)

        # New Code: Suggestion Shubham A
        # make predict function prepares the predict function before it being actually called.
        # This is used for thread safe execution
        model_rpn._make_predict_function()
        model_classifier._make_predict_function()
        # End New Code

        model_rpn.compile(optimizer='sgd', loss='mse')
        model_classifier.compile(optimizer='sgd', loss='mse')

        all_imgs = []

        classes = {}

        bbox_threshold = 0.8

        visualise = True

        cv2_image = img

        X, ratio = format_img(cv2_image, C)

        if K.image_dim_ordering() == 'tf':
            X = np.transpose(X, (0, 2, 3, 1))

        # get the feature maps and output from the RPN
        [Y1, Y2, F] = model_rpn.predict(X)

        R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_dim_ordering(), overlap_thresh=0.7)

        # convert from (x1,y1,x2,y2) to (x,y,w,h)
        R[:, 2] -= R[:, 0]
        R[:, 3] -= R[:, 1]

        # apply the spatial pyramid pooling to the proposed regions
        bboxes = {}
        probs = {}

        for jk in range(R.shape[0] // C.num_rois + 1):
            ROIs = np.expand_dims(R[C.num_rois * jk:C.num_rois * (jk + 1), :], axis=0)
            if ROIs.shape[1] == 0:
                break

            if jk == R.shape[0] // C.num_rois:
                # pad R
                curr_shape = ROIs.shape
                target_shape = (curr_shape[0], C.num_rois, curr_shape[2])
                ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
                ROIs_padded[:, :curr_shape[1], :] = ROIs
                ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
                ROIs = ROIs_padded

            [P_cls, P_regr] = model_classifier_only.predict([F, ROIs])

            for ii in range(P_cls.shape[1]):

                if np.max(P_cls[0, ii, :]) < bbox_threshold or np.argmax(P_cls[0, ii, :]) == (P_cls.shape[2] - 1):
                    continue

                cls_name = class_mapping[np.argmax(P_cls[0, ii, :])]

                if cls_name not in bboxes:
                    bboxes[cls_name] = []
                    probs[cls_name] = []

                (x, y, w, h) = ROIs[0, ii, :]

                cls_num = np.argmax(P_cls[0, ii, :])
                try:
                    (tx, ty, tw, th) = P_regr[0, ii, 4 * cls_num:4 * (cls_num + 1)]
                    tx /= C.classifier_regr_std[0]
                    ty /= C.classifier_regr_std[1]
                    tw /= C.classifier_regr_std[2]
                    th /= C.classifier_regr_std[3]
                    x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
                except:
                    pass
                bboxes[cls_name].append(
                    [C.rpn_stride * x, C.rpn_stride * y, C.rpn_stride * (x + w), C.rpn_stride * (y + h)])
                probs[cls_name].append(np.max(P_cls[0, ii, :]))

        all_dets = []

        for key in bboxes:
            bbox = np.array(bboxes[key])

            new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.5)
            for jk in range(new_boxes.shape[0]):
                (x1, y1, x2, y2) = new_boxes[jk, :]

                (real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)

                cv2.rectangle(cv2_image, (real_x1, real_y1), (real_x2, real_y2),
                              (int(class_to_color[key][0]), int(class_to_color[key][1]), int(class_to_color[key][2])),
                              2)
                # if real_y1 > 55 and real_y2 < 498:
                coords = [real_x1, real_y1, real_x2, real_y2]
                label = get_label(coords, cv2_image)
                labels.append(label)

                cv2.rectangle(cv2_image, (real_x1, real_y1), (real_x2, real_y2), (
                    int(class_to_color[key][0]), int(class_to_color[key][1]), int(class_to_color[key][2])), 2)
                # cv2.imshow("window", cv2_image[real_y1:real_y2, real_x1:real_x2])
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()
                # print(cv2_image)
                if label == "checked":
                    cv2.rectangle(img, (real_x1, real_y1), (real_x2, real_y2), (
                    int(class_to_color[key][0]), int(class_to_color[key][1]), int(class_to_color[key][2])), 2)
                    box_number, party = gettext((real_x1 + real_x2) / 2, (real_y1 + real_y2) / 2, cv2_image)
                    checkboxes[party.upper()].append(box_number)
                    # boxes.append(box_number)
                    # parties.append(party)
    except Exception as E:
        logger.critical(f"Checbox Exception: {E}")
        print(f"Checkbox Exception: {E}")

    K.clear_session()
    return checkboxes