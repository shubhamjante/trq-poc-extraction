
from keras.models import Sequential
from keras.models import model_from_json
import numpy as np
import os

def predict(model, X):
    Y = model.predict(X)
    Y = np.argmax(Y, axis=1)
    Y = 'checked' if Y[0] == 0 else 'unchecked'
    return Y

if __name__ == '__main__':
    import sys
    img_dir = sys.argv[1]
    from get_dataset import get_img
    result = []
    for file in os.listdir(img_dir):
        img = get_img(img_dir + "\\" + file)       
        X = np.zeros((1, 64, 64, 3), dtype='float64')
        X[0] = img
        # Getting model:
        model_file = open('Data/Model/model.json', 'r')
        model = model_file.read()
        model_file.close()
        model = model_from_json(model)
        # Getting weights
        model.load_weights("Data/Model/weights.h5")
        Y = predict(model, X)
        result.append(Y)
    print(result)