 # Arda Mavi

from keras.models import Sequential
from keras.models import model_from_json
import sys
import numpy as np
from scipy.misc import imresize
import cv2

# def predict(model, X):
#     Y = model.predict(X)
#     Y = np.argmax(Y, axis=1)
#     Y = 'checked' if Y[0] == 0 else 'unchecked'
#     return Y

def get_label(coords, img):
    #img_dir = sys.argv[1]
    img = img[coords[1]:coords[3], coords[0]:coords[2]]
    try:
        img = imresize(img, (64, 64, 3))
    except Exception as E:
        return 'unchecked'

    # img = [cv2.resize(img, (64, 64))]
    X = np.zeros((1, 64, 64, 3), dtype='float64')
    X[0] = img

    # Getting model:
    model_file = open('./modules/segmentation/middle_section/Models/classifier/model.json', 'r')
    model = model_file.read()
    model_file.close()
    model = model_from_json(model)
    # Getting weights
    model.load_weights('./modules/segmentation/middle_section/Models/classifier/weights.h5')
    Y = model.predict(X)
    Y = np.argmax(Y, axis=1)
    Y = 'checked' if Y[0] == 0 else 'unchecked'
    return Y
