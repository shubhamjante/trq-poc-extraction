import cv2
from copy import deepcopy
import io
import base64
import matplotlib.pyplot as plt
import logging
import time
import os
import pandas as pd
from fuzzywuzzy import fuzz
import uuid
import re
import gc
import psutil
logger = logging.getLogger('docbyte')


__countries = ('Portugal', 'Alemanha', 'france', 'spain', 'espanha', 'panama', 'germany', 'luxemburgo', 'poland',
               'italy', 'italia', 'dinamarca', 'belgica', 'holanda', 'marrocos', 'bulgaria', 'granada',
               'austria', 'portugal', 'plonia', 'czech republic', 'monaco', 'labuan', 'hungria')


__car_make = ('MARUTI', 'MERCEDES BENZ', 'TATA', 'LEXUS','TOYOTA', 'BMW', 'BENTLY', 'AUDI', 'SUZUKI', 'NISSAN', 'CITRON',
              'FORD', 'OPEL', 'RENAULT', 'PEUGEOT', 'VOLKSWAGEN', 'VOLVO', 'ALFA ROMEO', 'LAND ROVER', 'RANGE ROVER',
              'KIA', 'HONDA', 'CHEVROLET', 'HYUNDAI', 'SEAT', 'SKODA', 'MINI COPPER', 'FIAT', 'MAZDA', 'DATSON',
              'MITSUBISHI', 'YAMAHA')


def load_image_from_local_path(path):
    t1 = time.time()
    if not os.path.isfile(path):
        logger.error("Unable to load an image from path: {}".format(path))
        raise Exception("Unable to load image")

    image = cv2.imread(path)
    logger.info("Time taken to load an image: {}".format(time.time() - t1))
    return image


def middle_part_white_overlay(img):
    height, width = img.shape[:2]
    white_image = cv2.rectangle(img, (23, 0), (174, height), (255, 255, 255), -1)
    return white_image


def crop_binary_image_data(img, x, y, w, h):
    cropped = img[y:h, x:w]
    return cv2.imencode('.png', deepcopy(cropped))[1].tobytes()


def get_binary_image(img):
    return cv2.imencode('.png', img)[1].tobytes()


def crop_image_with_co_ordinates(img, x, y, w, h):
    cropped = img[y:h, x:w]
    return deepcopy(cropped)


def image_uri_from_array(img, figsize=(100, 100)):
    """
    This method will return the image URI for provided image.

    Returns:
        str: base64 encoded uri for images
    """
    t1 = time.time()
    plot = io.BytesIO()
    plt.figure(figsize=figsize)
    plt.imshow(img)
    plt.savefig(plot, format='png')
    plot.seek(0)
    plot_url = base64.b64encode(plot.getvalue()).decode()
    plt.close()
    logger.info("Time required to generate image URI to plot: {}".format(time.time() - t1))
    return 'data:image/png;base64,{}'.format(plot_url)


def __edit_distance(text1, text2):
    return fuzz.token_sort_ratio(text1, text2)


def __get_similar_match(text):
    if text.lower() == 'pt':
        return 'portugal'


def get_similar_country(text):
    # print(f"Similar country")
    if text.lower() == 'pt':
        return 'portugal'

    data = pd.DataFrame()
    data['countries'] = __countries
    data['similarity'] = data['countries'].apply(lambda x: __edit_distance(text, x))
    data = data.sort_values(by='similarity', ascending=False)

    similarity = data.iloc[0]
    return similarity['countries'] if similarity['similarity'] >= 40 else text


def get_similar_car_make_type(text):
    # print(f"similar car: {text}")
    data = pd.DataFrame()
    data['cars'] = __countries
    data['similarity'] = data['cars'].apply(lambda x: __edit_distance(text, x))
    data = data.sort_values(by='similarity', ascending=False)

    similarity = data.iloc[0]
    return similarity['cars'] if similarity['similarity'] >= 40 else text


def write_image(image):
    name = str(uuid.uuid4())
    os.mkdir(f'./static/{name}/')
    cv2.imwrite(f'./static/{name}/{name}.jpg', image)
    logger.info('Image Saved. Generated folder: {}'.format(name))
    return name


def log_image(image, image_uuid, filename):
    assert isinstance(filename, str)
    if filename.endswith('.jpg') or filename.endswith('.png'):
        filename = filename.split('.')[0]

    cv2.imwrite(f'./static/{image_uuid}/{filename}.jpg', image)
    logger.info('Intermediate image logged successfully. File Path: {}/{}'.format(image_uuid, filename))


def correct_image(image, value):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    alpha = 1.5 # Contrast control (1.0-3.0)
    beta = 1.3 # Brightness control (0-100)

    adjusted = cv2.convertScaleAbs(img, alpha, beta)

    # cv2.imshow('original', image)
    # cv2.imshow('adjusted', adjusted)
    cv2.waitKey(0)

    return adjusted


def format_policy_number(data):
    assert isinstance(data, dict)
    policy_formats = {
        'A_Actual': data['party_a']['insurance']['policy_number'].strip(),
        'B_Actual': data['party_b']['insurance']['policy_number'].strip(),
        'A': re.findall(r'\d+', data['party_a']['insurance']['policy_number'].strip()),
        'B': re.findall(r'\d+', data['party_b']['insurance']['policy_number'].strip())
    }
    logger.info(f'Trying to format Policy_Number: {policy_formats}')

    # policy_number_A_formats = [re.findall('T\d+', policy_number_A), re.findall('A\d+', policy_number_A),
    #                            re.findall('E\d+', policy_number_A), re.findall('\d+', policy_number_A)]

    if policy_formats['A']:
        data['party_a']['insurance']['policy_number'] = ''.join(policy_formats['A'])
    else:
        data['party_a']['insurance']['policy_number'] = f"{policy_formats['A_Actual']} Not a valid policy number"
    if policy_formats['B']:
        data['party_b']['insurance']['policy_number'] = ''.join(policy_formats['B'])
    else:
        data['party_b']['insurance']['policy_number'] = f"{policy_formats['B_Actual']} Not a valid policy number"

    return data


def format_license_plate(data):
    assert isinstance(data, dict)
    license_formats = {
        'A_Actual': data['party_a']['vehicle']['registration_number'].strip(),
        'B_Actual': data['party_b']['vehicle']['registration_number'].strip(),
        'A': ''.join(re.findall('\\w+', data['party_a']['vehicle']['registration_number']))[:6],
        'B': ''.join(re.findall('\\w+', data['party_b']['vehicle']['registration_number']))[:6]
    }
    try:
        data['party_a']['vehicle']['registration_number'] = '-'.join([
            license_formats['A'][0:2], license_formats['A'][2:4], license_formats['A'][4:]
        ])
        data['party_b']['vehicle']['registration_number'] = '-'.join([
            license_formats['B'][0:2], license_formats['B'][2:4], license_formats['B'][4:]
        ])
    except Exception as E:
        print(f"Unable to format License Plate: {E}")
        logger.error(f"Unable to format License Plate: {E}")

    # if len(license_formats['A']) == 6:
    #     data['party_a']['vehicle']['registration_number'] = '-'.join(
    #         [license_formats['A'][i:i+2] for i in range(0, len(license_formats['A'])-1, 2)]
    #     )
    # if len(license_formats['B']) == 6:
    #     data['party_b']['vehicle']['registration_number'] = '-'.join(
    #         [license_formats['B'][i:i+2] for i in range(0, len(license_formats['B'])-1, 2)]
    #     )

    return data


def do_garbage_collection():
    logger.debug(f"Current Memory Utilization: {psutil.virtual_memory()}")
    # gc.set_debug(gc.DEBUG_LEAK)
    # logger.debug(f"Garbage Collector Stats: {gc.get_stats()}")
    gc.collect()
    logger.debug(f"Garbage Collection Finished and Stats:{gc.get_stats()}")
