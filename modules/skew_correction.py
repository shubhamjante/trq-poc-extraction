import cv2
import numpy as np
import imutils
import os
import errno
import io
import matplotlib.pyplot as plt
import base64

class AlignImage:
    def __init__(self, image_path):
        self.__MAX_MATCHES = 80000
        self.__GOOD_MATCH_PERCENT = 0.005
        # self.__reference_image_path = './references/reference_image.png'
        self.__image_path = image_path

        if not os.path.isfile(self.__image_path):
            raise Exception(f"File Not Found. Please check given path: {os.path.abspath(self.__image_path)}")

    def __image_features(self):
        reference_image_path = './modules/references/reference_image.png'
        reference = cv2.imread(reference_image_path)
        print(reference.shape)
        reference = imutils.resize(reference, height=1000)
        reference = cv2.cvtColor(reference, cv2.COLOR_BGR2GRAY)

        image = cv2.imread(self.__image_path)
        image = imutils.resize(image, height=1000)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Detect ORF features and key-points
        orb = cv2.ORB_create(self.__MAX_MATCHES)
        reference_keypoints, reference_descriptors = orb.detectAndCompute(reference, None)
        image_keypoints, image_descriptors = orb.detectAndCompute(image, None)

        # Match features
        matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
        matches = matcher.match(image_descriptors, reference_descriptors, None)

        # Sort matches
        matches.sort(key=lambda x: x.distance, reverse=False)

        # Remove not so good matches
        good_matches = int(len(matches) * self.__GOOD_MATCH_PERCENT)
        matches = matches[:good_matches]

        # Extract location of good matches
        points1 = np.zeros((len(matches), 2), dtype=np.float32)
        points2 = np.zeros((len(matches), 2), dtype=np.float32)

        for i, match in enumerate(matches):
            points1[i, :] = image_keypoints[match.queryIdx].pt
            points2[i, :] = reference_keypoints[match.trainIdx].pt


        # Find homography
        h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

        # Use homography
        height, width = reference.shape
        im1Reg = cv2.warpPerspective(image, h, (width, height))

        return im1Reg, h

    # def __image_corrections(self):
    #     aligned_image, h = self.__image_features()
    #     lab = cv2.cvtColor(aligned_image, cv2.COLOR_BGR2LAB)
    #     l, a, b = cv2.split(lab)
    #     clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    #     cl = clahe.apply(l)
    #     limg = cv2.merge((cl, a, b))
    #     final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    #
    #     return final

    def __plot_image(self, aligned_image):
        aligned_image = cv2.cvtColor(aligned_image, cv2.COLOR_GRAY2RGB)
        plot = io.BytesIO()
        plt.figure(figsize=(100, 100))
        plt.imshow(aligned_image)
        plt.savefig(plot, format='png')
        plot.seek(0)
        plot_url = base64.b64encode(plot.getvalue()).decode()
        plt.close()
        return 'data:image/png;base64,{}'.format(plot_url)

    def align_images(self):
        aligned_image, h = self.__image_features()
        cv2.imwrite('./static/aligned.jpg', aligned_image)

        return './static/aligned.jpg', self.__plot_image(aligned_image)


# if __name__ == '__main__':
#     obj = AlignImage('./references/sample.jpg').align_images()

