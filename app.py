from flask import Flask, request, render_template, jsonify, session
from modules.application import extract_content
from modules import preprocessing
import logger
import time
import cv2
import numpy as np
import os
import gc
# os.environ["CUDA_VISIBLE_DEVICES"]="-1"

app = Flask(__name__)


@app.route('/')
def index():
    return 'Welcome User.'


@app.route('/api/v3/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'GET':
        return jsonify({'test': 'test'})
    elif request.method == 'POST':
        # preprocessing.do_garbage_collection()

        req = request
        np_arr = np.frombuffer(req.data, np.uint8)
        image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

        image_path = preprocessing.write_image(image)

        t1 = time.time()
        output = extract_content(image, image_path)
        try:
            output = preprocessing.format_policy_number(output)
            output = preprocessing.format_license_plate(output)
        except Exception as E:
            log.info(f'Error while processing request: {E}')

        log.info(f"Total time required for images extraction: {time.time() - t1}")
        output['total_time'] = time.time() - t1

        # preprocessing.do_garbage_collection()
        gc.collect()
        # session.clear()
        return jsonify(output)


@app.before_request
def fun():
    import psutil
    import gc
    print('Before Request Processing Run Garbage Collector')
    gc.collect()
    log.debug(f"Before Processing Memory Footprint: {psutil.virtual_memory()}")


if __name__ == '__main__':
    log = logger.get_logger('docbyte')
    app.run(host='0.0.0.0', port=3000, debug=True)
