import logging


FORMATTER = "%(asctime)s; %(levelname)s; %(filename)s; %(funcName)s; %(lineno)d; %(message)s"
DATEFORMAT = "%m/%d/%Y %I:%M:%S %p"


def __get_file_handler(mode):
    handler = logging.FileHandler('./logs/app.log')
    handler.setLevel(mode)
    formatter = logging.Formatter(FORMATTER, datefmt=DATEFORMAT)
    handler.setFormatter(formatter)

    return handler


def get_logger(logger_name='docbyte', logger_mode='debug'):
    assert logger_mode.upper() in ['DEBUG', 'WARN', 'WARNING', 'INFO', 'ERROR', 'CRITICAL']
    mode = {
        'debug': logging.DEBUG,
        'warn': logging.WARN,
        'warning': logging.WARNING,
        'info': logging.INFO,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }
    # logging.basicConfig(level=mode[logger_mode.lower()])
    logger = logging.getLogger(logger_name)
    logger.setLevel(mode[logger_mode.lower()])
    logger.addHandler(__get_file_handler(mode[logger_mode]))

    return logger
