# from modules.application import extract_content
# from modules import preprocessing
# import logger
# import glob
# import time
# import pandas as pd
# import collections
#
#
# def flatten(d, parent_key='', sep='_'):
#     items = []
#     for k, v in d.items():
#         new_key = parent_key + sep + k if parent_key else k
#         if isinstance(v, collections.MutableMapping):
#             items.extend(flatten(v, new_key, sep=sep).items())
#         else:
#             items.append((new_key, v))
#     return dict(items)
#
#
# log = logger.get_logger('docbyte')
# folder_path = './static/Good_Accuracy'
# files = glob.glob(f'{folder_path}/*.png')
# results = list()
#
# # Define Start and end
# start = 0
# end = 38 # always -1
# files = files[start:end]
#
#
# for file in files:
#     print(f'- Processing {file}')
#     t1 = time.time()
#     try:
#         image = preprocessing.load_image_from_local_path(file)
#         output = extract_content(image)
#         log.info(f"Total time required for images extraction: {time.time() - t1}")
#         output['total_time'] = time.time() - t1
#         output['file_path'] = file
#
#         results.append(flatten(output))
#     except Exception as E:
#         print(f'  Unable to process.')
#         continue
#
#
# pd.DataFrame(results).to_csv('results_38.csv', index='false')

# Testing POST Request
import requests
import glob
import json

headers = {
    'content-type': 'image/png'
}

URL = 'http://172.31.45.19:3000/api/v3/predict'
folder_path = './static/images/100 Test Images'
files = glob.glob(f"{folder_path}/*.png")

for image in files:
    file_name = image.split('\\')[-1].split('.')[0]
    print(f"- Processing: {file_name}")
    img = open(image, 'rb').read()
    response = requests.post(URL, data=img, headers=headers)
    with open('./static/JSON_Output_Local/{}.json'.format(file_name), 'w') as f:
        json.dump(response.text, f)
    print(f" Processing Completed: {file_name}")
    # break
