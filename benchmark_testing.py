from modules import preprocessing
from modules import application
import os
import glob
import collections
import pandas as pd

test_images = './static/images/100 Test Images/'


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


files = glob.glob(f'{test_images}/*.png')
cols = 	['header_country', 'header_location', 'header_date', 'checkboxes_A',
           'checkboxes_B', 'party_a_vehicle_make', 'party_a_vehicle_registration_number',
           'party_a_vehicle_country', 'party_a_insurance_policy_number', 'party_a_insurance_name',
           'party_b_vehicle_make', 'party_b_vehicle_registration_number', 'party_b_vehicle_country',
           'party_b_insurance_policy_number', 'party_b_insurance_name', 'sign_A', 'sign_B',
           'file_path']

# pd.DataFrame(columns=cols).to_csv('100_test_results.csv', index=False)
results = list()
for file in files:
    try:
        # file_name = file.split('\\')[-1].split('.')[0]
        image = preprocessing.load_image_from_local_path(file)
        output = application.extract_content(image, file)
        output['file_path'] = file
        output = preprocessing.format_policy_number(output)
        output = preprocessing.format_license_plate(output)
        results.append(flatten(output))
        pd.DataFrame([flatten(output)]).to_csv('100_test_results.csv', mode='a')
        print(f'Saved Extracted Output: {file}')
        # break
    except Exception as E:
        print(f"Exception: {E}")
        continue

pd.DataFrame(results).to_csv('results.csv', columns=cols)

